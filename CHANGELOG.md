# Semantic Versioning Changelog

## [1.0.1](https://gitlab.com/pss-x/support/gitlab-ci-templates/renovate/config/compare/v1.0.0...v1.0.1) (2025-02-26)


### Bug Fixes

* registryURL errors fixed ([8f9b305](https://gitlab.com/pss-x/support/gitlab-ci-templates/renovate/config/commit/8f9b30524f4eaa4d8ea5f2623a6bd7829a17cef2))
* renovate json updated [skip ci] ([cec6df5](https://gitlab.com/pss-x/support/gitlab-ci-templates/renovate/config/commit/cec6df5d30b68d2941272014bd6281f69d17f6d1))

# 1.0.0 (2025-02-24)


### Bug Fixes

* rebranding applied ([bbb0584](https://gitlab.com/pss-x/support/gitlab-ci-templates/renovate/config/commit/bbb0584279710e562c992177ffe29d3da8283d80))
* rebranding applied [skip ci] ([cad3ad1](https://gitlab.com/pss-x/support/gitlab-ci-templates/renovate/config/commit/cad3ad134ab6e79423b239cdd1984c5be526f695))

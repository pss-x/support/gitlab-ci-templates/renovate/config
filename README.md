# Renovate Config

This project provides common renovate configuration presets catered for pssx projects. You can extend these presets in your modules or application projects.

For more details, see the upstream [config preset documentation](https://docs.renovatebot.com/config-presets).

## Available presets

### default

It can be extended without explicitly providing the filename.

Example:

```json
{
  "extends": ["local>pss-x/support/gitlab-ci-templates/renovate/config:default.json5"]
}
```

### nuget

These presets are related to the management of nuget packages of modules of GridLab.PSSX applications and their ddd layers.
It creates a single MR per set of internal dependencies as well as a set of external dependencies for minor and patch levels.

Example:

```json
{
  "extends": [
    "local>pss-x/support/gitlab-ci-templates/renovate/config:default.json5",
    "local>pss-x/support/gitlab-ci-templates/renovate/config:nuget.json5"
  ]
}
```

### npm

These presets are related to managing npm packages of modules of GridLab.PSSX applications.
It creates a single MR per set of internal dependencies as well as a set of external dependencies for minor and patch levels.

Example:

```json
{
  "extends": [
    "local>pss-x/support/gitlab-ci-templates/renovate/config:default.json5",
    "local>pss-x/support/gitlab-ci-templates/renovate/config:npm.json5"
  ]
}
```

### docker

These presets are related to managing docker and docker compose files in the projects.

Example:

```json
{
  "extends": [
    "local>pss-x/support/gitlab-ci-templates/renovate/config:default.json5",
    "local>pss-x/support/gitlab-ci-templates/renovate/config:docker.json5"
  ]
}
```

### regex-yaml

Enables comment-based regex parsing for automated updates of YAML files;
for example, with dependency versions defined as Ansible or GitLab CI variables.

This preset provides a custom regex manager for these variables to be used with specially formatted comments. The special comments must have the following format (parentheses mean the value is optional):

```yaml
# renovate: datasource=<datasource> depName=<depName>(|<lookupName>) (versioning=<versioning>)
<some_dependency>_version: "0.0.1"
```

* The yaml variable must have suffix `_version`, or use the fixed key string `version`
* Quoting the value is optional (single or double quotes are accepted if quoting)
* `<datasource>` is a mandatory renovate [datasource](https://docs.renovatebot.com/modules/datasource/).
* `<depName>` is a mandatory dependency name valid for the `<datasource>`; if an
  optional `<lookupName>` is specified after a `|` character, then `<depName>`
  is a free text dependency name (but recommended to use usual characters
  to avoid problems with regexes). This is useful when the same dependency exists
  in different variables that must evolve independently.
* `<lookupName>` is an optional dependency name that must be valid for the
  `<datasource>`.
* `<versioning>` is an optional renovate [versioning](https://docs.renovatebot.com/configuration-options/#versioning)
  value that must be valid for the `<datasource>`; if not present `semver` will
  be used.

Typically, you would use a `depName` in your configuration that mirrors the nested yaml variable name, e.g.:

```yaml
common:
  # renovate: datasource=pypi depName=common.awscli|awscli
  awscli_version: "1.18.162"
```

If a dependency should not be auto-updated, you can make renovate ignore it with something like:

```yaml
common:
  # FROZEN renovate: datasource=pypi depName=common.awscli|awscli
  awscli_version: "1.18.162"
```

### regex-shell

This preset behaves exactly the same as regex-yaml, but for shell scripts:

* it matches files with a `*.sh` pattern,
* it expects shell variables defined with a case-insensitive suffix, e.g. `_VERSION` or `_version`.

The special comments follow the same format as above (parentheses mean the value is optional):

```sh
# renovate: datasource=<datasource> depName=<depName>(|<lookupName>) (versioning=<versioning>)
<some_dependency>_VERSION="1.0.0"
```

Or, as also shown above, a simple example (note the optional quotes):

```sh
# renovate: datasource=github-releases depName=gitleaks/gitleaks
GITLEAKS_VERSION=1.0.0
```

### regex-yml

Enables certain docker images automated updates of YML files at `templates` folders;

### regex-docker

Sometimes tools are installed as part of a Dockerfile and the version is either not pinned or the version is never updated. 
Renovate is able to find and update such versions by using the [regex manager](https://docs.renovatebot.com/modules/manager/regex/) and even provides a [preset](https://docs.renovatebot.com/presets-regexManagers/#regexmanagersdockerfileversions).

Consider the following Dockerfile:
```docker
FROM ubuntu:22.04
# renovate: datasource=github-releases depName=aquasecurity/trivy
ARG TRIVY_VERSION=0.30.4
RUN curl --location --fail https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64Bit.tar.gz | \
        tar -xzC /usr/local/bin trivy
```

The comment as well as the build argument are matched by the regex manager and Renovate can extract the current version and propose updates.

With the introduction of BuildKit, Docker requires to specify the syntax of the Dockerfile to enable the corresponding language features:
```docker
#syntax=docker/dockerfile:1.4.2
FROM ubuntu:22.04
```

Regex manager matches the comment containing the syntax definition and enable Renovate to propose update

### kubernetes

The kubernetes preset provides a common `fileMatch` config as well as custom versioning schemes needed for specific kubernetes dependencies.

### terraform

Enables automated updates of terraform files;

### gitlab-ci

Enables automated updates of gitlab pipeline related files;

## Have a Problem or Suggestion ?

Please see a list of features and bugs under issues If you have a problem which is not listed there, please let us known.